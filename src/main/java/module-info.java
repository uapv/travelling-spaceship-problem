module com.uapv.tp2.travellingspaceship {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.swing;
    requires TSPModel.PtiDeb;
    requires gs.core;
    requires gs.ui.javafx;
    requires opencsv;
    requires java.base;

    opens com.uapv.tp2.travellingspaceship to javafx.fxml;
    exports com.uapv.tp2.travellingspaceship;
}
