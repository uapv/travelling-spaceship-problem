/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uapv.tp2.travellingspaceship;

/**
 *
 * @author philtasticguy
 */
public class BasicNode {
    private int id;
    private int x;
    private int y;
    
    public int getId() {
        return id;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }

    void setX(int x) {
        this.x = x;
    }

    void setY(int y) {
        this.y = y;
    }
    
    public BasicNode(int id, int x, int y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }
}
