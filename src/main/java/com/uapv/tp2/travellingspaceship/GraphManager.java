/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uapv.tp2.travellingspaceship;

import javafx.scene.input.MouseEvent;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.ui.fx_viewer.FxViewPanel;
import org.graphstream.ui.fx_viewer.FxViewer;
import org.graphstream.ui.fx_viewer.util.FxMouseManager;
import org.graphstream.ui.geom.Point3;
import org.graphstream.ui.graphicGraph.GraphicElement;
import org.graphstream.ui.javafx.FxGraphRenderer;

/**
 * Singleton factory to create our node with consistency.
 * 
 * Java implementation from Refactoring Guru:
 * https://refactoring.guru/design-patterns/singleton/java/example#example-2
 */

public final class GraphManager {

    public interface NodeEventListener
    {
        public void onNodeAdded(NodeEvent event);
        public void onNodeRemoved(NodeEvent event);
    }
    
    private final String CSS_NODES =
        "graph {" +
        "padding: 40px;" +
        "}" +
        "node {" +
        "size-mode: fit;" +
        "shape: rounded-box;" +
        "fill-color: rgba(255,255,255,128);" +
        //"stroke-mode: plain;" +
        "padding: 3px;" +
        //"text-alignment: under;" +
        "icon-mode: at-left;" +
        "icon: url('" + getResourcePath("image/icons8-earth-planet-35.png") + "');" +
        "}";
    
    private Graph current;
    private FxViewPanel viewPanel;
    
    public Graph getGraph() {
        return current;
    }
    
    public FxViewPanel getViewPanel() {
        return viewPanel;
    }
    
    public GraphManager(Graph graph) {
        current = graph;
        createFxViewPanel();
        resetGraph();
    }
    
    private String getResourcePath(String relativePath) {
        var path = getClass().getClassLoader().getResource(relativePath);
        if (path.toString().startsWith("file")) {
            return path.getPath();
        }
        else {
            return path.toString();
        }
    }
    
    private int getMaxNodeId() {
        int maxById = 0;
        
        if (current.nodes().count() > 0) {
            maxById = current.nodes()
                .mapToInt(n -> Integer.parseInt(n.getId()))
                .max()
                .getAsInt() + 1;
        }
        
        return maxById;
    }
    
    private void createFxViewPanel() {
        FxViewer viewer = new FxViewer(current, FxViewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
        viewPanel = (FxViewPanel)viewer.addDefaultView(false, new FxGraphRenderer());
        
        // Set the camera properties so that the Graph Units are between 0-100
        // and prevent the camera from auto adjusting the view.
        //viewPanel.getCamera().setAutoFitView(false);
        viewPanel.getCamera().setGraphViewport(0, 0, 101, 101);
        viewPanel.getCamera().setViewCenter(50, 50, 0);
        
        // Handle mouse events on the Graph's ViewPanel.
        viewPanel.setMouseManager(new FxMouseManager() {
            @Override
            protected void elementMoving(GraphicElement element, MouseEvent event) {
                // Do nothing! Prevents users from dragging nodes around...
                return;
            }

            @Override
            protected void mouseButtonPress(MouseEvent event) {
                // Convert the click's coordinate on the Graph into Graph Units
                // for GraphStream.
                Point3 gu = view.getCamera().transformPxToGu(
                    event.getX(), 
                    event.getY()
                );
                
                if (gu.x > 0 && gu.y > 0 && gu.x < 101 && gu.y < 101) {
                    BasicNode node = new BasicNode(
                        getMaxNodeId(),
                        (int)(gu.x),
                        (int)(gu.y)
                    );

                    // Alert others that the node has been added.
                    //
                    // NOTE: The nodes are added to the Graph via the TableView's
                    // ListChangedListener for its observable list.
                    viewPanel.fireEvent(new NodeEvent(node));
                }
            }
        });
    }
    
    public void resetGraph() {
        current.clear();
        current.setAttribute("ui.stylesheet", CSS_NODES);
    }
    
    private void setNodeAttributes(Node n, BasicNode node) {
        n.setAttribute("ui.label", n.getId());
        n.setAttribute("ui.frozen", true);
        n.setAttribute("xyz", node.getX(), node.getY(), 0);
    }
    
    public Node addNode(BasicNode node) {
        Node n = current.addNode("" + node.getId());
        setNodeAttributes(n, node);
        return n;
    }
    
    public Node editNode(BasicNode selectedNode) {
        Node n = current.getNode("" + selectedNode.getId());
        setNodeAttributes(n, selectedNode);
        return n;
    }

    public void removeNode(BasicNode node) {
        current.removeNode("" + node.getId());
    }
    
    public Edge addEdge(int edgeId, int nodeStartId, int nodeEndId) {
        Edge e = current.addEdge("" + edgeId, "" + nodeStartId, "" + nodeEndId);
        return e;
    }
    
    public Edge addEdge(Edge edge) {
        Edge e = current.addEdge(
            edge.getId(),
            edge.getNode0().getId(),
            edge.getNode1().getId()
        );
        return e;
    }
    
    public void removeEdge(int edgeId) {
        current.removeEdge("" + edgeId);
    }
    
    public void shutdown() {
        if (viewPanel != null) {
            viewPanel.getViewer().close();
        }
    }
}
