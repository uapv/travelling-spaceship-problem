/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uapv.tp2.travellingspaceship;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

/**
 * Singleton factory to create our node with consistency.
 * 
 * Java implementation from Refactoring Guru:
 * https://refactoring.guru/design-patterns/singleton/java/example#example-2
 */
public final class NodeFactory {
    private static volatile NodeFactory instance;
    
    private NodeFactory() {
    }
    
    public static NodeFactory getInstance() {
        NodeFactory result = instance;
        if (result != null) {
            return result;
        }
        synchronized(NodeFactory.class) {
            if (instance == null) {
                instance = new NodeFactory();
            }
            return instance;
        }
    }
    
    private void setNodeAttributes(Node n, BasicNode node) {
        n.setAttribute("ui.label", n.getId());
        n.setAttribute("xyz", node.getX(), node.getY(), 0);
    }
    
    public Node createNode(Graph graph, BasicNode node) {
        Node n = graph.addNode("" + node.getId());
        setNodeAttributes(n, node);
        return n;
    }
    
    public Node editNode(Graph graph, BasicNode selectedNode) {
        Node n = graph.getNode("" + selectedNode.getId());
        setNodeAttributes(n, selectedNode);
        return n;
    }
}
