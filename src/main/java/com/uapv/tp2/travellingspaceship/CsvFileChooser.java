/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uapv.tp2.travellingspaceship;

import java.io.File;
import javafx.stage.FileChooser;
import javafx.stage.Window;

/**
 *
 * @author philtasticguy
 */
public class CsvFileChooser {
    
    public static File showOpenDialog(Window parent) {
        FileChooser fc = new FileChooser();
        fc.setTitle("Charger les nodes d'un fichier CSV...");
        fc.setInitialDirectory(
            new File(".")
        );
        fc.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("Fichiers CSV", "*.csv")
        );
        
        return fc.showOpenDialog(parent);
    }
    
    public static File showSaveDialog(Window parent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Enregistrer les nodes dans un fichier CSV...");
        fileChooser.setInitialDirectory(
            new File(".")
        );
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("Fichiers CSV", "*.csv")
        );
        
        return fileChooser.showSaveDialog(parent);
    }
    
}
