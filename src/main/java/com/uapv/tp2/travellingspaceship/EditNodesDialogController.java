/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uapv.tp2.travellingspaceship;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.fx_viewer.FxViewPanel;

/**
 * FXML Controller class
 *
 * @author tuanct1997
 */
public class EditNodesDialogController implements Initializable {
    @FXML
    public TableView<BasicNode> tvNodes;
    @FXML
    public TableColumn<BasicNode,Integer> tcId;
    @FXML
    public TableColumn<BasicNode,Integer> tcX;
    @FXML
    public TableColumn<BasicNode,Integer> tcY;
    @FXML
    public AnchorPane apGraph;
    @FXML
    public TextField txtId;
    @FXML
    public Spinner<Integer> spinX;
    @FXML
    public Spinner<Integer> spinY;
    @FXML
    public Button btnAdd;
    @FXML
    public Button btnRemove;
    @FXML
    public Button btnEdit;
    @FXML
    public Button btnCancel;
    @FXML 
    public Button btnRandom;
    
    private GraphManager currentGraph;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tvNodes.getItems().addListener((ListChangeListener.Change<? extends BasicNode> c) -> {
            while (c.next()) {
                if (c.wasAdded()) {
                    c.getAddedSubList().forEach(node -> 
                        currentGraph.addNode(node));
                }
                else if (c.wasRemoved()) {
                    c.getRemoved().forEach(node -> 
                        currentGraph.removeNode(node));
                }
            }
        });
        tvNodes.setRowFactory(tv -> {
            TableRow<BasicNode> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getButton().equals(MouseButton.PRIMARY) && 
                    !row.isEmpty() && 
                    !tvNodes.isDisable())
                {
                    btnEdit.setDisable(false);
                }
            });
            return row;
        });
        
        currentGraph = new GraphManager(new SingleGraph("Current Solution"));
        addFxViewPanel(apGraph, currentGraph.getViewPanel());
        
        currentGraph.getViewPanel().addEventHandler(NodeEvent.NODE_ADDED, event -> {
            tvNodes.getItems().add(event.getNode());
            setNextNodeId();
        });
        
        tcId.setReorderable(false);
        tcId.setCellValueFactory(new PropertyValueFactory<>("Id"));
        
        tcX.setReorderable(false);
        tcX.setCellValueFactory(new PropertyValueFactory<>("X"));
        
        tcY.setReorderable(false);
        tcY.setCellValueFactory(new PropertyValueFactory<>("Y"));
        
        spinX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100, 1));
        spinY.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100, 1));
        
        btnEdit.setDisable(true);
        btnCancel.setDisable(true);
    }
    
    private void addFxViewPanel(AnchorPane target, FxViewPanel panel) {
        if (target.getChildren().size() > 0) {
            target.getChildren().clear();
        }
        
        panel.getViewer().disableAutoLayout();
        
        target.getChildren().add(panel);
        AnchorPane.setTopAnchor(panel, 0.0);
        AnchorPane.setRightAnchor(panel, 0.0);
        AnchorPane.setBottomAnchor(panel, 0.0);
        AnchorPane.setLeftAnchor(panel, 0.0);
    }
    
    public ArrayList<BasicNode> getNodes() {
        return new ArrayList<>(tvNodes.getItems());
    }
    
    public void setNodes(ArrayList<BasicNode> nodes) {
        tvNodes.getItems().addAll(nodes);
        setNextNodeId();
    }
    
    @FXML
    private void onAddNodeClicked() {
        BasicNode node = new BasicNode(
            Integer.parseInt(txtId.getText()),
            spinX.getValue(),
            spinY.getValue()
        );
        tvNodes.getItems().add(node);
        
        setNextNodeId();
        spinX.getValueFactory().setValue(1);
        spinY.getValueFactory().setValue(1);
    }

    private void setNextNodeId() {
        int maxById = 0;
        
        var nodes = tvNodes.getItems();
        if (nodes.size() > 0) {
            maxById = nodes.stream()
                .max(Comparator.comparing(BasicNode::getId))
                .get().getId() + 1;
        }
        
        txtId.setText("" + maxById);
    }
    
    @FXML
    private void onRemoveNodeClicked() {
        BasicNode selectedNode = tvNodes.getSelectionModel().getSelectedItem();
        if (selectedNode != null) {
            tvNodes.getItems().remove(selectedNode);
            setNextNodeId();
        }
    }
    
    @FXML
    private void onEditClicked() { 
        BasicNode selectedNode = tvNodes.getSelectionModel().getSelectedItem();
        if (btnEdit.getText().equals("Modifier") && selectedNode != null) {
            tvNodes.setDisable(true);
            btnEdit.setText("Sauvegarder");
            btnCancel.setDisable(false);
            btnAdd.setDisable(true);
            btnRemove.setDisable(true);
            
            txtId.setText("" + selectedNode.getId());
            spinX.getValueFactory().setValue(selectedNode.getX());
            spinY.getValueFactory().setValue(selectedNode.getY());
        }
        else if (btnEdit.getText().equals("Sauvegarder") && selectedNode != null) {
            tvNodes.setDisable(false);
            selectedNode.setX(spinX.getValue());
            selectedNode.setY(spinY.getValue());
            tvNodes.refresh();
            
            currentGraph.editNode(selectedNode);
            
            btnEdit.setDisable(tvNodes.getSelectionModel().getSelectedItem() != null);
            btnEdit.setText("Modifier");
            btnCancel.setDisable(true);
            btnAdd.setDisable(false);
            btnRemove.setDisable(false);
            
            setNextNodeId();
            spinX.getValueFactory().setValue(1);
            spinY.getValueFactory().setValue(1);
        }
    }
    
    @FXML
    private void onCancelClicked() {
        tvNodes.setDisable(false);
        btnEdit.setDisable(tvNodes.getSelectionModel().getSelectedItem() != null);
        btnEdit.setText("Modifier");
        btnCancel.setDisable(true);
        btnAdd.setDisable(false);
        btnRemove.setDisable(false);

        setNextNodeId();
        spinX.getValueFactory().setValue(1);
        spinY.getValueFactory().setValue(1);
    }
    
    @FXML
    private void onRandomClicked() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GenerateNodesDialog.fxml"));
        Parent root = fxmlLoader.load();
        
        GenerateNodesDialogController controller = fxmlLoader.getController();
            
        Dialog dialog = new Dialog();
        dialog.setTitle("Générer de nouveaux noeuds...");
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        dialog.getDialogPane().setContent(root);
        dialog.setResizable(true);
        
        Stage stage = (Stage)dialog.getDialogPane().getScene().getWindow();
        stage.setMinWidth(275);
        stage.setMinHeight(100);
        
        dialog.showAndWait()
            .filter(response -> response == ButtonType.OK)
            .ifPresent(response -> {
                generateNodes(controller.spinNodes.getValue());
            });
    }

    private void generateNodes(int amount) {
        Random random = new Random();
        
        for (int i = 0; i < amount; i++) {
            int x, y;
            boolean isTooClose;
            do {
                x = random.ints(1, 100).findFirst().getAsInt();
                y = random.ints(1, 100).findFirst().getAsInt();
                
                isTooClose = false;
                for (var n : tvNodes.getItems()) {
                    if ((Math.abs(n.getX() - x) < 15) && (Math.abs(n.getY() - y) < 15)) {
                        isTooClose = true;
                        break;
                    }
                }
            } while(isTooClose);
            
            BasicNode node = new BasicNode(
                Integer.parseInt(txtId.getText()),
                x,
                y
            );
            tvNodes.getItems().add(node);
            
            setNextNodeId();
        }
    }
}
