/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uapv.tp2.travellingspaceship;

import TSPModel_PtiDeb.*;
import TSPModel_PtiDeb.TSPModel_PtiDeb.ActionType;
import com.opencsv.exceptions.CsvException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.fx_viewer.FxViewPanel;

/**
 * FXML Controller class
 *
 * @author philtasticguy
 */
public class MainFrameController implements Initializable, Observer {
    @FXML 
    public AnchorPane currentGraphPane;
    @FXML 
    public AnchorPane bestSolutionGraphPane;
    @FXML 
    public Button btnStart;
    @FXML 
    public Button btnPause;
    @FXML 
    public Button btnRestart;
    @FXML
    public Slider sliderSpeed;
    @FXML 
    public Button btnEdit;
    @FXML 
    public Button btnLoad;
    @FXML 
    public Button btnSave;
    @FXML 
    public Label lblDistanceCurrent;
    @FXML 
    public Label lblDistanceBest;
    
    private enum SimulationState {
        Initialized,
        Started,
        Paused,
        Finished
    };
    
    private ArrayList<BasicNode> nodes;
    private Thread modelThread;
    private TSPModel_PtiDeb model;
    
    private GraphManager currentGraph;
    private GraphManager bestGraph;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        model = new TSPModel_PtiDeb(this);
        nodes = new ArrayList<BasicNode>();
        
        currentGraph = new GraphManager(new SingleGraph("Current Solution"));
        addFxViewPanel(currentGraphPane, currentGraph.getViewPanel());
        
        bestGraph = new GraphManager(new SingleGraph("Best Solution"));
        addFxViewPanel(bestSolutionGraphPane, bestGraph.getViewPanel());
        
        setUiState(SimulationState.Initialized);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        var m = (TSPModel_PtiDeb)o;
        
        // Run these GUI updates on the UI thread because Java was complaining...
        Platform.runLater(new Runnable() {
            @Override public void run() {
                // Round up to a single decimal point.
                double roundedDistance = Math.round(m.getDistanceTotale() * 10.0) / 10.0;
                
                lblDistanceCurrent.setText("" + roundedDistance);
                
                // Only update the best solution when a new one is found.
                if (m.getAction() == ActionType.NewBest) {
                    lblDistanceBest.setText("" + roundedDistance);
                }
            }
        });
        
        switch (m.getAction()) {
            case Add:
                System.out.println("[ADD]    " + m.getSegmentID() + " (" + m.getSegmentP1() + ", " + m.getSegmentP2() + ")");
                currentGraph.addEdge(m.getSegmentID(), m.getSegmentP1(), m.getSegmentP2());
                break;
                
            case Remove:
                System.out.println("[REMOVE] " + m.getSegmentID() + " (" + m.getSegmentP1() + ", " + m.getSegmentP2() + ")");
                currentGraph.removeEdge(m.getSegmentID());
                break;
                
            case NewBest:
                System.out.println("[BEST] New best solution found!");
                bestGraph.resetGraph();
                nodes.forEach(node -> bestGraph.addNode(node));
                currentGraph.getGraph().edges().forEach(e -> bestGraph.addEdge(e));
                break;
                
            case Finish:
                System.out.println("[DONE] Finished!");
                setUiState(SimulationState.Finished);
                break;
                
            default:
                break;
        }
        
        // Slow down the execution speed based on the slider's value.
        try {
            long ms = (long)(sliderSpeed.getValue() * 1000);
            System.out.println("[WAIT] " + ms + "ms");
            
            synchronized(m) {
                m.wait(ms);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    
    private void setUiState(SimulationState state) {
        switch (state) {
            case Initialized:
                btnStart.setDisable((nodes != null && nodes.size() == 0));
                btnPause.setDisable(true);
                btnRestart.setDisable(true);
                btnEdit.setDisable(false);
                btnLoad.setDisable(false);
                btnSave.setDisable(true);
                break;
                
            case Started:
                btnStart.setDisable(true);
                btnPause.setDisable(false);
                btnRestart.setDisable(true);
                btnEdit.setDisable(true);
                btnLoad.setDisable(true);
                btnSave.setDisable(true);
                break;
                
            case Paused:
                btnStart.setDisable(false);
                btnPause.setDisable(true);
                btnRestart.setDisable(false);
                btnEdit.setDisable(false);
                btnLoad.setDisable(false);
                btnSave.setDisable(false);
                break;
                
            case Finished:
                btnStart.setDisable(false);
                btnPause.setDisable(true);
                btnRestart.setDisable(true);
                btnEdit.setDisable(false);
                btnLoad.setDisable(false);
                btnSave.setDisable(false);
                break;
                
            default:
                break;
        }
    }
    
    private void addFxViewPanel(AnchorPane target, FxViewPanel panel) {
        if (target.getChildren().size() > 0) {
            target.getChildren().clear();
        }
        
        target.getChildren().add(panel);
        AnchorPane.setTopAnchor(panel, 0.0);
        AnchorPane.setRightAnchor(panel, 0.0);
        AnchorPane.setBottomAnchor(panel, 0.0);
        AnchorPane.setLeftAnchor(panel, 0.0);
    }

    private void StartSimulation() {
        modelThread = new Thread(model);
        modelThread.start();
    }

    private void UnpauseSimulation() {
        model.setPause(false);
        synchronized(model) {
            model.notify();
        }
    }

    private void ResetSimulation() {
        if (modelThread != null) {
            modelThread.stop();
        }
        
        model.clear();
        currentGraph.resetGraph();
        bestGraph.resetGraph();
        
        nodes.forEach(node -> {
            model.addPoint(node.getId(), node.getX(), node.getY());
            currentGraph.addNode(node);
        });
    }
    
    public void shutdown() {
        System.out.println("[EXIT] Shutting down the application.");
        
        // Stop the background thread immediately.
        if (modelThread != null) {
            modelThread.stop();
        }
        
        currentGraph.shutdown();
        bestGraph.shutdown();
    }
    
    @FXML
    private void onStartClicked() {
        if (model.getPause()) {
            System.out.println("[UNPAUSE] Unpausing the simulation.");
            UnpauseSimulation();
        }
        else {
            System.out.println("[START] Starting the simulation.");
            StartSimulation();
        }
        
        setUiState(SimulationState.Started);
    }
    
    @FXML
    private void onPauseClicked() throws InterruptedException {
        System.out.println("[PAUSE] Pausing the simulation.");
        
        model.setPause(true);
        
        setUiState(SimulationState.Paused);
    }
    
    @FXML
    private void onRestartClicked() {
        System.out.println("[RESET] Resetting the simulation.");
        
        ResetSimulation();
        StartSimulation();
            
        setUiState(SimulationState.Started);
    }
    
    @FXML
    private void onEditClicked() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EditNodesDialog.fxml"));
//        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Test.fxml"));
        Parent root = fxmlLoader.load();
        
        EditNodesDialogController controller = fxmlLoader.getController();
        controller.setNodes(nodes);
            
        Dialog dialog = new Dialog();
        dialog.setTitle("Modifier les nodes du graphe...");
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        dialog.getDialogPane().setContent(root);
        dialog.setResizable(true);
        
        Stage stage = (Stage)dialog.getDialogPane().getScene().getWindow();
        stage.setMinWidth(725);
        stage.setMinHeight(600);
        
        dialog.showAndWait()
            .filter(response -> response == ButtonType.OK)
            .ifPresent(response -> {
                if (controller.getNodes() != null && controller.getNodes().size() > 0) {
                    nodes = controller.getNodes();
                    ResetSimulation();
                    setUiState(SimulationState.Initialized);
                }
            });
        
        btnSave.setDisable((controller.getNodes().size() == 0));
    }
    
    @FXML
    private void onLoadClicked() throws IOException, CsvException {
        File file = CsvFileChooser.showOpenDialog(
            btnSave.getScene().getWindow()
        );
        
        if (file != null) {
            System.out.println("[LOAD] Loading nodes from file: " + file.getPath());
            nodes = NodeStore.loadFromCsv(file.getPath());
            ResetSimulation();
            setUiState(SimulationState.Initialized);
        }
    }
    
    @FXML
    private void onSaveClicked() throws IOException, CsvException {
        File file = CsvFileChooser.showSaveDialog(
            btnSave.getScene().getWindow()
        );
        
        if (file != null) {
            System.out.println("[SAVE] Saving nodes to file: " + file.getPath());
            NodeStore.saveToCsv(file.getPath(), nodes);
        }
    }
}
