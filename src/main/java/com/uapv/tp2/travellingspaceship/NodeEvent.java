/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uapv.tp2.travellingspaceship;

import javafx.event.Event;
import static javafx.event.Event.ANY;
import javafx.event.EventType;

/**
 *
 * @author philtasticguy
 */
public class NodeEvent extends Event {

    private BasicNode node;
    
    public BasicNode getNode() {
        return node;
    }
    
    public static final EventType<NodeEvent> NODE_ADDED = new EventType(ANY, "NODE_ADDED");

    public NodeEvent(BasicNode node) {
        super(NodeEvent.NODE_ADDED);
        this.node = node;
    }

}
