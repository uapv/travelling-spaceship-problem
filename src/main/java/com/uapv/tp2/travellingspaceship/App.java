package com.uapv.tp2.travellingspaceship;

import com.opencsv.exceptions.CsvException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.application.Platform;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    
    @Override
    public void start(Stage stage) throws IOException, CsvException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("MainFrame.fxml"));
        
        scene = new Scene(fxmlLoader.load(), 820, 620);
        MainFrameController controller = fxmlLoader.getController();
        
        stage.setTitle("Les petits débrouillards - Simulateur de problème du voyageur de commerce");
        stage.setScene(scene);
        stage.setMinWidth(850);
        stage.setMinHeight(670);
        stage.setOnHidden(e -> {
            controller.shutdown();
            Platform.exit();
        });
        stage.show();
    }
    
    public static void main(String[] args) {
        launch();
    }

}