/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uapv.tp2.travellingspaceship;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages BasicNodes.
 */
public class NodeStore {
    
    /**
     * Load ArrayList of BasicNode from a CSV file.
     * @param filePath Path to the CSV file to load. 
     * @return ArrayList of BasicNode containing all the nodes from the CSV file.
     * @throws IOException
     * @throws CsvException 
     */
    public static ArrayList<BasicNode> loadFromCsv(String filePath) throws IOException, CsvException
    {
        var nodes = new ArrayList<BasicNode>();
        
        try (var reader = new CSVReader(new FileReader(filePath))) {
            List<String[]> r = reader.readAll();
            r.forEach(line -> nodes.add(parseLineToNode(line)));
        }
        
        return nodes;
    }
    
    /**
     * Save ArrayList of BasicNode to a CSV file.
     * @param filePath Path to the CSV file to load. 
     * @param nodes ArrayList of BasicNode containing all nodes from the CSV file.
     * @throws IOException
     * @throws CsvException 
     */
    public static void saveToCsv(String filePath, ArrayList<BasicNode> nodes) throws IOException, CsvException
    {
        var lines = new ArrayList<String[]>();
        for (var node : nodes) {
            lines.add(new String[] { "" + node.getId(), "" + node.getX(), "" + node.getY()});
        }
        
        try (var writer = new CSVWriter(new FileWriter(filePath))) {
            writer.writeAll(lines);
        }
    }
    
    /**
     * Parses a single line of the CSV file into a BasicNode.
     * @param line String array representing the split line from the CSV file.
     * @return BasicNode representing the String[] received.
     * @throws NumberFormatException 
     */
    private static BasicNode parseLineToNode(String[] line) throws NumberFormatException
    {
        if (line == null) {
            throw new IllegalArgumentException("The 'line' parameter cannot be NULL.");
        }
        else if (line.length != 3) {
            throw new IllegalArgumentException("The 'line' parameter must have a length of '3'.");
        }
        
        var node = new BasicNode(
            Integer.parseInt(line[0]), 
            Integer.parseInt(line[1]),
            Integer.parseInt(line[2])
        );
        
        System.out.println(node.toString());
        
        return node;
    }
    
}
