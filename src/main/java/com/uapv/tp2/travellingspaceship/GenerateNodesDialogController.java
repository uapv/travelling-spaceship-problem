/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uapv.tp2.travellingspaceship;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;

/**
 * FXML Controller class
 *
 * @author tuanct1997
 */
public class GenerateNodesDialogController implements Initializable {
    @FXML
    public Spinner<Integer> spinNodes;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        spinNodes.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 25, 1));
    }
}
